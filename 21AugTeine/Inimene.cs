﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _21AugTeine
{
    class Inimene
    {
        private string eesnimi;
        private string perenimi;
        public DateTime Sünniaeg;

        public string GetEesnimi() => eesnimi; //Getter Funktsioon
        public void SetEesnimi(string nimi) => eesnimi = TitleCase(nimi); // setter neetod

        public string Eesnimi
        {
            get => eesnimi; //lühversioon get funktsioonist
            set => eesnimi = TitleCase(value); 
        }
        public string Perenimi
        {
            get { return perenimi; }
            set { perenimi = TitleCase(value); }
        }

        public string Täisnimi => eesnimi + " " + perenimi;  // Readonly property. Muuta ei saa, annab ainult vasted vastavalt nimedele.


        public int Vanus() //funktsiooni ees tuleb alati kasutada tüüpi (int, string..), mis tüüpi see vastus on.
        {
            return (DateTime.Today - Sünniaeg).Days * 4 / 1461;
        }
        public void Prindi()
        {
            Console.WriteLine($"On inimene, kelle nimi on {Eesnimi} {Perenimi}");
        }
        public static string TitleCase(string nimi)
        {
            string[] nimed = nimi.Split(' ');
            for (int i = 0; i < nimed.Length; i++)
            {
                nimed[i] = nimed[i].Length == 0 ? "":
                    nimed[i].Substring(0, 1).ToUpper() + nimed[i].Substring(1).ToLower();


            }
            return string.Join(" ", nimed);
        }

        }

}
