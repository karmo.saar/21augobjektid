﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _21AugTeine
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene karmo = new Inimene
            {
                Eesnimi = "karmo",
                Perenimi = "saar",
                Sünniaeg = new DateTime(1987, 03, 13)
            };

            //List<Inimene> inimesed = new List<Inimene>
            //{
            //    karmo,
            //    new Inimene{Eesnimi = "Ants", Vanus=40},
            //    new Inimene{Eesnimi = "Peeter", Vanus=22},
            //    new Inimene{Eesnimi = "Kalle", Vanus=12},
            //};

            //inimesed.Add(new Inimene { Eesnimi = "Malle", Vanus = 77 });
            //foreach (var x in inimesed) Console.WriteLine(x);

            Console.WriteLine(karmo.Vanus());
            karmo.Prindi();
            Console.WriteLine(Liida(4, 7));
            Console.WriteLine(Liida(y: 4, x: 7));
            Console.WriteLine(Liida(5, 4, 2));

            Console.WriteLine(Summa(2, 5, 6, 7, 1)); // liidab kokku ja saab 21
            int[] arvud = { 1, 2, 3, 4, 5, 6, 7, 8, 9 }; //Liidab kokku ja saab 45
            Console.WriteLine(Summa(arvud));

            int a = 3; int b = 7; // siin toimub suuna vahetus. Alguses ühtepidi
            Console.WriteLine($"a- {a}, b-{b}");
            Vaheta(ref a, ref b); //Siis suuna muuutus ja teistpidi. 
            Console.WriteLine($"a- {a}, b-{b}");

            string c = "Karmo";
            string d = "Saar"; // siin toimub suuna vahetus. Alguses ühtepidi
            Console.WriteLine($"esimene- {c}, teine-{d}");
            Swap(ref c, ref d); //Siis suuna muuutus ja teistpidi. 
            Console.WriteLine($"esimene- {c}, teine-{d}");

            int f = 5;
            Console.WriteLine(Factorial(f)); //siin korrutatakse 5*4*3*2*1=120. Rekursiivne faktoriaal. 
        }
        static int Liida(int x, int y)
        {
            return x + y;

        }
        static int Liida(int x, int y, int z) //overload funtsioon, mis on sama nimeaga, aga erinevate parameetritega. Sõltuvalt parameetrite arv, suund, järjekord ja tüüp, sellest sõltub signatuur.
        { //Funtsioonil peab olema unikaalne signatuur (parameetrite arv, tüüp, suund ja järjekord.) Parameetrite nimed ei oma tähtsust. 
            return x + Liida(y, z);
        }
        static int Summa(int x) => x;
        //static int Summa(int x, int y) => x + y;
        //static int Summa(int x, int y int z) => x + y + z;
        static int Summa(params int[] arvud)
        {
            int summa = 0; foreach (var x in arvud) summa += x;
            return summa;
        }
        static void Vaheta(ref int yks, ref int teine) //parameetri suund
        {
            int ajutine = yks;
            yks = teine;
            teine = ajutine;
        }
        static void Swap<T>(ref T yks, ref T teine) //parameetri suund, siin muudab stringe. T=tüübi parameeter, generic funtksioon.
        {
            T ajutine = yks;
            yks = teine;
            teine = ajutine;
        }
        static int Factorial(int x)
        {
            if (x < 2) return x;
            return x * Factorial(x - 1);
        }

    }
}

